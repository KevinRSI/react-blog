# The project

I had to make a blog-like site with authentification and user pages.

## Realisation

I started with an use case to apprehend what I needed to create in terms of classes & functionalities, and a wireframe to help me visualize what I wanted to do.

![use case](public/Readme_Img/blog%20use%20case.jpg)

![wireframe](public/Readme_Img/maquette%20blog.png)

Then i did a class diagram to know how many tables I should make and what their relations would be.

![class diagram](public/Readme_Img/UML%20Blog%20(1).png)

For the Auth I used bcrypt to hash the passwords and JWT to send a token to the user after a successful login.
To validate the user input upon register and login I made a JOI validation with differents schemas for every possible inputs.

![Register](public/Readme_Img/register-blog.png)

In the front part I use protected routes to redirect users to the homepage if they're not authenticated or if their roles doesn't allow them to access certain areas.

![protected](public/Readme_Img/protectedId.png)

### [Backend](https://gitlab.com/KevinRSI/node-blog)

### [Final result](https://kevinrsi.gitlab.io/react-blog/)
