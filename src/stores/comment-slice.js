import { createSlice } from "@reduxjs/toolkit";
import { CommentService } from "../shared/Comment-Services";


const commentSlice = createSlice({
    name: "comment",
    initialState: {
        comment: []
    },
    reducers: {
        allComments(state, {payload}){
            state.comment = payload
        }
    }
})

export const getAllComm = (id, limit, offset) => async (dispatch) =>{
    try {
    let data = await new CommentService().getComments(id, limit, offset);
    dispatch(allComments(data))
    } catch (error) {
        console.log(error);
    }
}

export const {allComments} = commentSlice.actions

export default commentSlice.reducer
