import { createSlice } from '@reduxjs/toolkit';


const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        modal: false,
        switch: true,
    },
    reducers: {
        changeModal(state, { payload }) {
            state.modal = payload
        },
        logOrRegister(state, { payload }) {
            state.switch = payload
        }
    }

});

export const toggleModal = (param) => (dispatch) => {
    dispatch(changeModal(param))
}   

export const toggleSwitch = (param) => (dispatch) => {
    dispatch(logOrRegister(param))
}




export const { changeModal, logOrRegister } = modalSlice.actions
export default modalSlice.reducer;