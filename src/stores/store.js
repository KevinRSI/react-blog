import { configureStore } from '@reduxjs/toolkit'
import articleSlice from './article-slice'
import authSlice from './Auth-slice'
import commentSlice from './comment-slice'
import modalSlice from './modal-slice'

export const store = configureStore({
    reducer: {
        auth : authSlice,
        art: articleSlice,
        modal: modalSlice,
        comm: commentSlice
    }
})