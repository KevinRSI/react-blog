import { createSlice } from '@reduxjs/toolkit';
import { ArticleService } from '../shared/Article-Services';


const articleSlice = createSlice({
    name: 'article',
    initialState: {
        article: null,
        thisArticle: null,
        new: {},
        feedback: '',
        userArt: null,
        updateArt: {},
        upFeedback: '',
    },
    reducers: {
        allArticle(state, { payload }) {
            state.article = payload
        },
        thisArticle(state, { payload }) {
            state.thisArticle = payload
        },
        NewArticle(state, { payload }) {
            state.new = payload
        },
        addFeedback(state, { payload }) {
            state.feedback = payload
        },
        userArticle(state, {payload}){
            state.userArt = payload
        },
        updateArticle(state, {payload}){
            state.updateArt = payload
        },
        updateFeedback(state, {payload}){
            state.upFeedback = payload
        }
    }

});

export const getAllArticle = (limit, offset) => async (dispatch) => {
    try {
        let data = await new ArticleService().getArticle(limit, offset);
        if (data) {
            dispatch(allArticle(data));
        }
    } catch (error) {
        console.log(error);
    }
}

export const oneArticle = (id) => async (dispatch) => {
    try {
        let data = await new ArticleService().getOne(id);
        if (data) {
            dispatch(thisArticle(data));
        }
    } catch (error) {
        console.log(error);
    }
}

export const sendArticle = (item) => async (dispatch) => {
    try {
        let data = await new ArticleService().AddArticle(item);
        if (data) {
            return dispatch(addFeedback('Article created !'))
        } else {
            return dispatch(addFeedback('There is an issue with your article please try again'))
        }
    } catch (error) {
        console.log(error);
    }
}
export const upArticle = (item) => async (dispatch) => {
    try {
        let data = await new ArticleService().update(item);
        if (data) {
            return dispatch(updateFeedback('Updated !'))
        } return dispatch(updateFeedback('error'))
    } catch (error) {
        console.log(error);
    }
}

export const articleByUser = (id) => async (dispatch) => {
    try {
        let data = await new ArticleService().getByUser(id)
        if (data) {
            dispatch(userArticle(data));
        }
    } catch (error) {
        console.log(error);
    }
}

export const { allArticle, thisArticle, NewArticle, addFeedback, userArticle, updateArticle, updateFeedback } = articleSlice.actions
export default articleSlice.reducer;