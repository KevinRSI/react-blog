import { createSlice } from '@reduxjs/toolkit';
import { UserService } from '../shared/User-Services';
const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: null,
        registerFeedback: null,
        lasts: null
    },
    reducers: {
        login(state, { payload }) {
            state.user = payload
        },
        logout(state) {
            state.user = null;
            localStorage.removeItem('token');
        },
        loginFeedback(state, { payload }) {
            state.loginFeedback = payload
        },
        registerFeedback(state, { payload }) {
            state.registerFeedback = payload;
        },
        lastUsers(state, { payload }) {
            state.lasts = payload
        }
    }

});

export const refreshLast = () => async (dispatch) => {
    try {
        let data = await new UserService().lastUsers();
        dispatch(lastUsers(data))
    } catch (error) {
        console.log(error);
    }
}

export const logWithToken = () => async (dispatch) => {
    try {
        let token = localStorage.getItem('token')
        if (token) {
            let data = await new UserService().fetchUser();
            if (data) {
                dispatch(login(data))
                
            }
        }
    } catch (error) {
        dispatch(logout())
    }
}

export const loginFunction = (user) => async (dispatch) => {
    try {
        dispatch(loginFeedback(''))
        let data = await new UserService().logUser(user);
        if (data) {
            dispatch(login(data))
            dispatch(loginFeedback('Connected !'))
        } else {
            dispatch(loginFeedback('Wrong email or password'))
        }
    } catch (error) {

        console.log(error);
    }
}

export const registerFunction = (data) => async (dispatch) => {
    try {
        dispatch(registerFeedback(''))
        let {user, token} = await new UserService().registerUser(data);
        if (user) {
            dispatch(registerFeedback('User registered !'))
            localStorage.setItem('token', token);
            dispatch(login(user))
            return true
        } else {
            dispatch(registerFeedback('Email or Pseudo already taken'))
            return false
        }
    } catch (error) {
        console.log(error);
    }
}

export const { login, logout, loginFeedback, registerFeedback, lastUsers } = authSlice.actions;

export default authSlice.reducer;