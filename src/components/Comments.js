import { MDBCard, MDBCardBody, MDBCardText } from "mdb-react-ui-kit"
import moment from "moment";
import Avatar from 'react-avatar';
import { useSelector } from "react-redux";

export const Comment = ({ data }) => {
    let actUser = useSelector(state => state.auth.user)
    return (
        <>{data && data.map((value, index) => {
            return <MDBCard className='m-2' key={value.commentId}>
                <MDBCardBody className="row">
                    <div className="col-lg-2 col-sm-3 col-12 py-2 mx-auto row text-center justify-content-center">
                        <div className="col-sm-12 col-6">
                            <Avatar src={value.user.avatar} name={value.user.pseudo} size="80" className='border' />
                        </div>
                        <div className="col-6 col-sm-12">
                            <p className={value.user.role === 'admin' ? 'text-danger' : 'text-primary'}>{value.user.pseudo}</p>
                            <small >{moment(value.date).format('ll')}</small>
                        </div>
                    </div>
                    <MDBCardText className="col-sm-9 col-lg-9 col-12 mt-3 mt-sm-1 text-start my-auto">{value.contenu}</MDBCardText>
                    <div className='col-lg-1 col-12 d-flex flex-lg-column justify-content-lg-between justify-content-around mt-2 mt-lg-0'>
                        { actUser && ( actUser.role === 'admin' || actUser.id === value.user.id) && <button className="btn btn-info btn-sm m-1"><i className="bi-pencil-square fa-lg"></i></button>}
                       {(actUser && actUser.role === 'admin') && <button className="btn btn-danger btn-sm m-1"><i className="bi-trash"></i></button>}
                    </div>

                </MDBCardBody>
            </MDBCard>
        })
        }
        </>
    )
}