import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { LogModal } from './LogModal';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../stores/Auth-slice';
import { MDBDropdown, MDBDropdownMenu, MDBDropdownToggle, MDBDropdownItem, MDBBtn, MDBNavbarToggler, MDBIcon, MDBCollapse } from 'mdb-react-ui-kit';
import { toggleModal } from '../stores/modal-slice';

export const Nav = () => {
    const [showBasic, setShowBasic] = useState(false);

    const logged = useSelector(state => state.auth.user);
    let dispatch = useDispatch();

    return (
        <nav className="navbar sticky-top  navbar-expand-md navbar-light bg-light ">
            <div className="container-fluid ">
                <MDBNavbarToggler
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                    onClick={() => setShowBasic(!showBasic)}
                >
                    <MDBIcon icon='bars' fas />
                </MDBNavbarToggler>
                <MDBCollapse navbar show={showBasic} className="h-25 align-items-center justify-conten-between row ">
                    <div className="col-sm-6 col-md-8 col-2  me-sm-0 me-3">
                        <Link className="username  text-reset" to='/'>
                            <MDBBtn color='light' className='btn-sm btn-link border  me-3'>
                            <i className="bi bi-house username me-md-1"></i>
                                <span className="d-none username d-md-inline">Home</span>
                            </MDBBtn>
                        </Link>
                    </div>
                    {logged ?
                        <div className="col-sm-5  col-md-4 col-lg-4 col-4 d-md-flex justify-content-end">
                            <MDBDropdown group className='shadow-0 btn-group '>
                                <Link to="/write" className="btn-sm btn btn-info border me-md-3 ms-3 w-auto order-sm-0 order-2"> <i className=" bi-pencil-square me-md-1"></i> <span className="d-none username d-md-inline"> write</span> </Link>
                                <Link  className='text-reset btn-sm btn btn-light d-flex border align-items-center' to='/account'><span className="username">{logged.pseudo}</span></Link>
                                <MDBDropdownToggle color='light' className='btn-sm border' split> <i className="bi-person-circle text-reset ms-1 username"></i></MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem className="">
                                        <button className="dropdown-item btn bg-danger text-light text-center" type="button" onClick={() => { dispatch(logout()) }}>Disconnect <i className="bi-box-arrow-right fa-sm ms-2"></i></button>
                                    </MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>

                        </div>
                        :
                        <div className="col-6  col-md-4  d-md-flex justify-content-end">
                            <MDBBtn onClick={() => dispatch(toggleModal(true))} className="btn-dark">Login | Register</MDBBtn>
                        </div>}



                </MDBCollapse>
            </div>
            <LogModal />
        </nav>
    )
}