
import { useEffect } from 'react';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import {
    MDBModal,
    MDBModalDialog,
    MDBModalContent,
    MDBModalHeader,
    MDBModalBody
} from 'mdb-react-ui-kit';
import { loginFunction, registerFunction } from '../stores/Auth-slice';
import { toggleModal, toggleSwitch } from '../stores/modal-slice';

export const LogModal = () => {
    let modal = useSelector(state => state.modal.modal)
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    let switchLog = useSelector(state => state.modal.switch)
    let regFeed = useSelector(state => state.auth.registerFeedback);
    let logFeed = useSelector(state => state.auth.loginFeedback);
    let dispatch = useDispatch()


    async function onLogin(data) {
        dispatch(loginFunction(data))
        
    }

    useEffect(()=>{
        if (logFeed === 'Connected !') {
            dispatch(toggleModal(false))
        }
        if (regFeed === "User registered !") {
            dispatch(toggleModal(false));
        }
    }, [logFeed, dispatch, regFeed])

    async function onRegister(data) {
        dispatch(registerFunction(data))
        
    }

    function changeLog(param) {
        dispatch(toggleSwitch(param));
        reset();
    }

    return (
        <>
            <MDBModal show={modal} backdrop={false} staticBackdrop={true} tabIndex='-1'>
                <MDBModalDialog >
                    <MDBModalContent>
                        <MDBModalHeader className="bg-info">
                            <button className={`btn w-50 m-1 ${switchLog ? 'btn-dark' : 'btn-outline-dark'}`} onClick={() => changeLog(true)}>Login</button>
                            <button className={`btn w-50 m-1 ${switchLog ? 'btn-outline-dark' : 'btn-dark'}`} onClick={() => changeLog(false)}>Register</button>
                        </MDBModalHeader>
                        <MDBModalBody>
                            {switchLog ? <div>
                                <h4 className='text-center'>Log to your account :</h4>
                                <form onSubmit={handleSubmit(onLogin)} className='row mt-1 g-3 justify-content-center'>

                                    <div className='col-lg-8 col-12'>
                                        <input type='text' placeholder='Pseudo' name='pseudo' className="form-control" {...register('pseudo', { required: true })} />
                                        {errors.pseudo && <p>{errors.pseudo.message}</p>}
                                    </div>
                                    <div className='col-lg-8 col-12'>
                                        <input type='password' placeholder='Password' className='form-control' {...register("password", { required: true })} />
                                        {errors.password && <p>{errors.password.message}</p>}
                                    </div>

                                    <button type="submit" className="btn mt-4 me-1 btn-info col-5">Sign In</button>
                                    <div className='col-12 text-center'><button className='btn mt-1 btn-dark col-3' onClick={()=>dispatch(toggleModal(false))}>Close</button></div>
                                </form> </div> :
                                <div>
                                    <h4 className='text-center'>Create your account now :</h4>
                                    <form onSubmit={handleSubmit(onRegister)} className='row mt-1 g-3 justify-content-center '>

                                        <div className='col-lg-8 col-12'>
                                            <input type="text" placeholder="Email" className='form-control' {...register("email", { required: true, pattern: /^\S+@\S+$/i })} />
                                        </div>
                                        <div className='col-lg-8 col-12'>
                                            <input type='text' placeholder='Pseudo' name='pseudo' className="form-control" {...register('pseudo', { required: true })} />
                                            {errors.pseudo && <p>{errors.pseudo.message}</p>}
                                        </div>
                                        <div className='col-lg-8 col-12'>
                                            <input type='password' placeholder='Password' className='form-control' {...register("password", { required: true })} />
                                            {errors.password && <p>{errors.password.message}</p>}

                                        </div>
                                        <button type="submit" className="btn mt-4 me-1 btn-info col-5">Sign Up</button>
                                        <div className='col-12 text-center'><button className='btn mt-1 btn-dark col-3' onClick={()=>dispatch(toggleModal(false))}>Close</button></div>
                                        
                                    </form>
                                </div>}
                        </MDBModalBody>
                    </MDBModalContent>
                </MDBModalDialog>
            </MDBModal>
        </>
    );
}