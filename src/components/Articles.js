import { MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBCardTitle } from "mdb-react-ui-kit";
import { useDispatch, useSelector } from "react-redux"
import moment from 'moment'
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { getAllArticle } from "../stores/article-slice";
import { allComments } from "../stores/comment-slice";

export const Articles = ({offset}) => {
    let list = useSelector(state => state.art.article);
    let dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getAllArticle(5, offset));
        dispatch(allComments(null))
    },[dispatch, offset])
    
    return (

        <div className="d-flex justify-content-center flex-wrap">
            {list ? list.map((value, index) => {
                return <Link className="mb-3 col-lg-10 col-12 m-2" key={value.articleId} to={"/article/" + value.articleId}>
                    <MDBCard  >
                        {value.image ? <MDBCardImage className="card-img" position='top' src={value.image} alt='...' /> : <MDBCardImage fluid className="card-img" position='top' src='placeholder/image-placeholder-wide.png' alt='...' />}
                        <MDBCardBody className="  text-dark">
                            <MDBCardTitle className='text-truncate'>{value.title}</MDBCardTitle>
                            <MDBCardText>
                                <small className='text-muted'>{moment(value.date).fromNow()}</small>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </Link>
            }) : <h5>Loading..</h5>}
        </div>

    )
}