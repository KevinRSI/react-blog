import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";


export function ProtectedRouteById({ children, ...rest }) {
    const user = useSelector(state => state.auth.user);
    const article = useSelector(state => state.art.updateArt)

    return (
        <Route {...rest}>
            {(user && article) ?
                <>
                    {
                        (user.id === article.userId) || (user.role === 'admin')
                            ? children
                            : <Redirect to='/' />
                    }
                </>
            : <Redirect to='/' />}
        </Route>
    );
}