import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_SERVER_URL;

export class UserService {
    async logUser(item) {
        try {
            let user = await axios.post('/user/login', {
                pseudo: item.pseudo,
                password: item.password
            });
            if (user.status === 200) {
                localStorage.setItem('token', user.data.token)
                return user.data.user
            }
        } catch (error) {
            return null
        }
    };

    async registerUser(item) {
        try {
            let user = await axios.post('/user/register', {
                email: item.email,
                pseudo: item.pseudo,
                password: item.password
            });
            if (user.status === 201) {
                
                return user.data
            } else if (user.status === 400) {
                return false
            }
        } catch (error) {
            return null
        }
    }

    async fetchUser() {
        let user = await axios.get('/user/find/acc');
        return user.data

    }

    async lastUsers(){
        let users = await axios.get('/user/last')
        return users.data
    }

    async updateUser(item){
        let user = await axios.patch('/user/'+item.id, item);
        return user
    }
}