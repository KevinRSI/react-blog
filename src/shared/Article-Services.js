import axios from "axios"
axios.defaults.baseURL = process.env.REACT_APP_SERVER_URL;


export class ArticleService {
    async getArticle(limit, offset){
        let articles = await axios.get('/article/', {
            params:{
                limit: limit,
                offset: offset
            }
        })
        return articles.data;
    }

    async getOne(id){
        let article = await axios.get('/article/'+id);
        return article.data

    }

    async AddArticle(item){
        let data = await axios.post('/article/',item);
        if (data.status === 201) {
            return true
        } else if (data.status === 400) {
            return false
        }
    }

    async delArticle(id){
        let data = await axios.delete('/article/'+id);
        if (data.status === 204) {
            return true
        } else {
            return false
        }
    }

    async getByUser(id){
        let articles = await axios.get('/article/user/'+id);
        return articles.data
    }

    async update(item){
        let data = await axios.patch('/article/'+item.articleId, item);
        if (data.status === 202) {
            return true
        } else if (data.status === 400) {
            return false
        }
    }
}
