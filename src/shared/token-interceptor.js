import axios from "axios";
import { toast } from "react-toastify";
import { logout } from "../stores/Auth-slice";
import { store } from '../stores/store'


axios.interceptors.request.use(config => {
    const token = localStorage.getItem('token');
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});


axios.interceptors.response.use(response => {
    return response;
}, error => {

    if (error.response.status === 403) {
        store.dispatch(logout());
        let customId = 'expired'
        toast.error('Access forbidden', {
            position: "top-right",
            toastId: customId,
            autoClose: 1999,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: undefined,
        })

    }
    return error;
});