import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_SERVER_URL;

export class CommentService {
    async getComments(id, limit, offset) {
        let comments = await axios.get('/comments/' + id, {
            params: {
                limit: limit,
                offset: offset
            }
        });
        return comments.data
    }
    async addComment(item) {
        let data = await axios.post('/comments/', item);
        if (data.status === 201) {
            return true
        } else if (data.status === 400) {
            return false
        }
    }
}