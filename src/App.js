
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Home } from './pages/Home';
import { Nav } from './components/Nav';
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { useEffect } from 'react';
import { logWithToken, refreshLast } from './stores/Auth-slice';
import { ArticleRead } from './pages/Read';
import { Write } from './pages/Write';
import { ProtectedRoute } from './components/Protected';
import { Account } from './pages/Account';
import { ProtectedRouteById } from './components/ProtectedbyId';
import { Update } from './pages/Update';


function App() {
    let logFeed = useSelector(state => state.auth.loginFeedback);
    let user = useSelector(state => state.auth.user);
    let regFeed = useSelector(state => state.auth.registerFeedback);
    let dispatch = useDispatch();

    if (logFeed === "Connected !" && user) {
        let customId = 'success'
        toast.success('Welcome ' + user.pseudo, {
            position: "bottom-right",
            toastId: customId,
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    } else if (logFeed === 'Wrong email or password') {
        let customId = 'wrong'
        toast.error(logFeed, {
            position: "bottom-right",
            toastId: customId,
            autoClose: 1999,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    };

    if (regFeed === 'User registered !') {
        let customId = 'success reg'
        toast.success(regFeed, {
            position: "bottom-right",
            toastId: customId,
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    } else if (regFeed === 'Email or Pseudo already taken') {
        let customId = 'wrong reg'
        toast.error(regFeed, {
            position: "bottom-right",
            toastId: customId,
            autoClose: 1999,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    };

    useEffect(() => {
        dispatch(logWithToken());
        dispatch(refreshLast());
    }, [dispatch])


    return (
        <Router basename={process.env.PUBLIC_URL}>
            <Nav></Nav>
            <ToastContainer />
            <div className="container-fluid">
                <Switch>
                    <Route exact path='/'>
                        <Home />
                    </Route>
                    <Route path='/article/:id'>
                        <ArticleRead />
                    </Route>
                    <ProtectedRoute path='/write'>
                        <Write/>
                    </ProtectedRoute>
                    <ProtectedRoute path='/account'>
                        <Account/>
                    </ProtectedRoute>
                    <ProtectedRouteById path='/update'>
                        <Update/>
                    </ProtectedRouteById>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
