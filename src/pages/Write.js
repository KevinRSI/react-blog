import { MDBInput, MDBInputGroup, MDBInputGroupText } from 'mdb-react-ui-kit';
import { useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown';
import MdEditor from 'react-markdown-editor-lite';
import 'react-markdown-editor-lite/lib/index.css';
import { useDispatch, useSelector } from 'react-redux';
import { addFeedback, allArticle, NewArticle, sendArticle } from '../stores/article-slice';
import * as filestack from 'filestack-js';
import moment from 'moment';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';


export const Write = () => {
    const [image, setImage] = useState(null)
    let change = useSelector(state => state.art.new);
    let articleFeed = useSelector(state => state.art.feedback);
    let articles = useSelector(state => state.art.article)
    let dispatch = useDispatch();
    let history = useHistory();

    const client = filestack.init('AREYv4BEcS7Kac5TzhVrGz');
    const options = {
        onFileSelected: file => {
            if (file.size > 1000 * 1000) {
                throw new Error('File too big, select something smaller than 1MB');
            }
        },
        transformations: {
            crop: {
                force: true,
                aspectRatio: 21 / 9
            }
        },
        accept: ["image/*"],
        uploadInBackground: false,
        onUploadDone: handleImage,



    };


    function handleImage(result) {
        setImage(result.filesUploaded[0])
        dispatch(NewArticle({ ...change, image: result.filesUploaded[0].url }))
    }

    function deleteImage() {
        client.remove(image.handle, { policy: 'eyJleHBpcnkiOjIyNDAxNzU2MDAsImNhbGwiOlsicmVtb3ZlIl19', signature: '4a30599c62c2645fefae24e278072f41bd760eb49154f9c80b96b50eaeb52356' });
        setImage(null)
    }


    function handleEditorChange({ html, text }) {
        dispatch(NewArticle({ ...change, text: text }))
    }

    useEffect(() => {
        if (articleFeed === 'Article created !') {
            let customId = 'success article'
            toast.success(articleFeed, {
                position: "bottom-right",
                toastId: customId,
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined
            })
            dispatch(allArticle([change, ...articles]))
            dispatch(NewArticle({}))
            dispatch(addFeedback(""))
            history.push('/')
        }
    }, [articleFeed, dispatch, history, change, articles])



    function handleSubmit() {
        if (!image) {
            dispatch(sendArticle({ ...change, image: null, date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') }));
        } else {
            dispatch(sendArticle({ ...change, date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') }));
        }

    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let update = { ...change, [name]: value };
        dispatch(NewArticle(update))

    }
    return (
        <div className='row g-3 justify-content-center my-3' >
            <div className="col-10 m-4 p-3 bg-light border border-info rounded row">
                <h2 className='text-center mb-3'><span className="border px-2">New Article</span></h2>
                <div className="col-md-6 col-12 my-2 ">
                    <MDBInput label='Title' name='title'
                        onChange={handleChange}
                        required ></MDBInput>
                </div>
                <div className='col-md-6 col-12 my-2 d-flex '>
                    {image === null ? <MDBInputGroup className='mb-3'>
                        <button name="image" className='w-75 btn border text-reset btn-sm btn-link ' onClick={() => client.picker(options).open()}>{'Click to add an image'}</button>
                        <MDBInputGroupText>image</MDBInputGroupText>
                    </MDBInputGroup> :
                        <MDBInputGroup className='mb-3'>
                            <MDBInputGroupText className="col-9">{image.filename}</MDBInputGroupText>
                            <button className='w-25 btn border text-reset btn-sm btn-link ' onClick={() => deleteImage()}>Delete</button>
                        </MDBInputGroup>
                    }
                </div>
                <MdEditor value={change.text} style={{ height: '30em' }} renderHTML={text => <ReactMarkdown>{text}</ReactMarkdown>} onChange={handleEditorChange} />
                <div className="col-12 mt-3 d-flex justify-content-center">
                    <button className="btn btn-info w-auto" onClick={() => handleSubmit()}>Submit</button>
                </div>

            </div>
        </div>
    )
}