import * as filestack from 'filestack-js';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBCardTitle, MDBInput, MDBTabs, MDBTabsContent, MDBTabsItem, MDBTabsLink, MDBTabsPane } from "mdb-react-ui-kit"
import moment from 'moment';
import { useEffect, useState } from "react";
import Avatar from "react-avatar";
import { useDispatch, useSelector } from "react-redux"
import { Link } from 'react-router-dom';
import { UserService } from "../shared/User-Services";
import { articleByUser } from '../stores/article-slice';
import { login } from "../stores/Auth-slice";

export const Account = () => {
    const user = useSelector(state => state.auth.user);
    const posts = useSelector(state => state.art.userArt);
    const [justifyActive, setJustifyActive] = useState('tab1');
    const [password, setPassword] = useState(null);
    const [validation, setValidation] = useState(null)
    const client = filestack.init('AREYv4BEcS7Kac5TzhVrGz');
    let dispatch = useDispatch();

    useEffect(()=>{
        dispatch(articleByUser(user.id))
    }, [dispatch, user.id])

    const options = {
        onFileSelected: file => {
            if (file.size > 1000 * 1000) {
                throw new Error('File too big, select something smaller than 1MB');
            }
        },
        transformations: {
            crop: {
                force: true,
                aspectRatio: 1/1
            }
        },
        accept: ["image/*"],
        uploadInBackground: false,
        onUploadDone: handleImage,
    };

    async function handleImage(result) {
        await new UserService().updateUser({avatar: result.filesUploaded[0].url, id: user.id  })
        dispatch(login({...user, avatar: result.filesUploaded[0].url }))
    }


    const handleJustifyClick = (value) => {
        if (value === justifyActive) {
            return;
        }

        setJustifyActive(value);
    };

    useEffect(() => {

    }, [])

    const handlePassword = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        let update = { ...password, [name]: value };
        setPassword(update);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (password.password === password.password2) {
            await new UserService().updateUser({ password: password.password, id: user.id })
            return setValidation({ message: 'Password updated !', color: 'success' })
        }
        setValidation({ message: 'Please enter the same password', color: 'danger' })
    }


    return (
        <div className="row justify-content-center">
            <h2 className="text-center p-1 mt-4"><span className="shadow-3 px-2 bg-white rounded ">{user.pseudo}</span></h2>
            <MDBCard className="col-12 col-md-6 shadow-4 mt-4 py-3">
                <MDBTabs justify className='mb-3'>
                    <MDBTabsItem>
                        <MDBTabsLink onClick={() => handleJustifyClick('tab1')} active={justifyActive === 'tab1'}>
                            <i className="bi-person-circle fa-lg"></i> Profile
                        </MDBTabsLink>
                    </MDBTabsItem>
                    <MDBTabsItem>
                        <MDBTabsLink onClick={() => handleJustifyClick('tab2')} active={justifyActive === 'tab2'}>
                            <i className="bi-shield-lock fa-lg"></i> Security
                        </MDBTabsLink>
                    </MDBTabsItem>
                    <MDBTabsItem>
                        <MDBTabsLink onClick={() => handleJustifyClick('tab3')} active={justifyActive === 'tab3'}>
                            <i className="bi bi-file-earmark-post"></i> Posts
                        </MDBTabsLink>
                    </MDBTabsItem>
                </MDBTabs>
                <div className='mt-3'>
                    <MDBTabsContent>
                        <MDBTabsPane show={justifyActive === 'tab1'}>
                            <div className='col-12 my-2 d-flex justify-content-center'>
                                <div className=''>
                                    <Avatar name={user.pseudo} src={user.avatar} className='border' />
                                </div>
                                <div className="ms-3">
                                    <p>Upload a new picture</p>
                                    <MDBBtn color="grey" outline onClick={() => client.picker(options).open()}>Upload</MDBBtn>
                                </div>
                            </div>
                        </MDBTabsPane>

                        <MDBTabsPane show={justifyActive === 'tab2'}>
                            <div className='col-12 my-2 text-center row justify-content-center'>
                                <div className=''>
                                    Change password
                                </div>
                                <form onSubmit={handleSubmit} className="w-50">
                                    <MDBInput onChange={handlePassword} type='password' name="password" label="new password" required className='my-2' ></MDBInput>
                                    <MDBInput onChange={handlePassword} type='password' name="password2" label="repeat new password" required></MDBInput>
                                    {validation && <small className={'text-' + validation.color}>{validation.message}</small>}<br />
                                    <MDBBtn type="submit" disabled={ (password && password.password2)  ? false : true} className='mt-2'>Submit</MDBBtn>

                                </form>
                            </div>
                        </MDBTabsPane>
                        <MDBTabsPane show={justifyActive === 'tab3'}>
                        <div className="d-flex justify-content-center flex-wrap">
            {posts ? posts.map((value, index) => {
                return <Link className="mb-3 col-lg-10 col-12 m-2" key={value.articleId} to={"/article/" + value.articleId}>
                    <MDBCard  >
                        {value.image ? <MDBCardImage className="card-img" position='top' src={value.image} alt='...' /> : <MDBCardImage fluid className="card-img" position='top' src='placeholder/image-placeholder-wide.png' alt='...' />}
                        <MDBCardBody className="  text-dark">
                            <MDBCardTitle className='text-truncate'>{value.title}</MDBCardTitle>
                            <MDBCardText>
                                <small className='text-muted'>{moment(value.date).fromNow()}</small>
                            </MDBCardText>
                        </MDBCardBody>
                    </MDBCard>
                </Link>
            }) : <p>You did not write anything yet</p>}
        </div>
                        </MDBTabsPane>
                    </MDBTabsContent>

                </div>
            </MDBCard>
        </div>
    )
}