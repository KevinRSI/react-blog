import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { oneArticle, thisArticle, updateArticle } from "../stores/article-slice";
import ReactMarkdown from "react-markdown"
import { ArticleService } from "../shared/Article-Services";
import { toast } from "react-toastify";
import { MDBInput } from "mdb-react-ui-kit";
import { getAllComm } from "../stores/comment-slice";
import { Comment } from "../components/Comments";
import { CommentService } from "../shared/Comment-Services";
import moment from "moment";


export const ArticleRead = () => {
    let { id } = useParams();
    let dispatch = useDispatch();
    let article = useSelector(state => state.art.thisArticle)
    let user = useSelector(state => state.auth.user);
    let comments = useSelector(state => state.comm.comment);
    const [change, setChange] = useState({contenu: ''});
    const [refreshed, setRefreshed] = useState(0);
    let history = useHistory();

    async function deleteArticle() {
        if (window.confirm('Do you really want to delete this article ?')) {
            let del = await new ArticleService().delArticle(id);
            if (del) {
                let customId = 'delArticle'
                toast.warning('Article deleted !', {
                    position: "bottom-right",
                    toastId: customId,
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                dispatch(thisArticle(null))
            }
        }
    }

    function handleSubmit(event) {
        let data = new CommentService().addComment(change);
        event.preventDefault();
        if (data) {
            setChange({contenu: ''})
            setRefreshed(refreshed+1);
        }

    }

    function updArt(){
        dispatch(updateArticle(article))
        history.push('/update')
    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        setChange({ ...change, [name]: value, date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), articleId : id })
    }

    useEffect(() => {
        dispatch(oneArticle(id))
        dispatch(getAllComm(id, 10, 0))
    }, [dispatch, id, refreshed])

    return (
        article ? <div className="row justify-content-center">
            <h2 className="col-12 text-center my-5 ">
                <span className=" bg-success p-2 text-light rounded">{article && article.title}</span> <br />
                {user && (article.userId === user.id || user.role === 'admin') ? <><button onClick={()=>updArt()} className="btn mt-3 btn-info btn-sm"><i className="bi-pencil-square fa-lg"></i></button>
                    <button onClick={() => deleteArticle()} className="btn ms-1 btn-sm btn-danger"><i className="bi-trash"></i></button></> : null}
            </h2>
            <div className="col-12 col-md-10 border p-3 my-2 border-info shadow-5 bg-light rounded article-img overflow-hidden">
                {article && <ReactMarkdown skipHtml={true} >{article.text}</ReactMarkdown>}
            </div>
            <div className="col-12 col-md-10 border p-3 my-2 border-success text-center shadow-4">
                <Comment data={comments} />
                <form onSubmit={handleSubmit} >
                    {user ? <MDBInput value={change.contenu} name="contenu" onInput={handleChange} textarea label="Answer the post" required></MDBInput> : <MDBInput disabled textarea label="You need to be authentified to do that"></MDBInput>}
                    <button type="submit" className="btn btn-info mt-3">Submit</button>
                </form>
            </div>

        </div> : <h6>Chargement en cours</h6>
    )
}