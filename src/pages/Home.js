import { MDBBadge, MDBCard, MDBCardBody, MDBCardTitle, MDBPagination, MDBPaginationItem, MDBSpinner } from "mdb-react-ui-kit"
import { useState } from "react";
import { useSelector } from "react-redux"
import { Articles } from "../components/Articles"



export const Home = () => {
    let last = useSelector(state => state.auth.lasts);
    let articles = useSelector(state => state.art.article)
    const [offset, setOffset] = useState(0);
    function increaseOffset() {
        if (articles.length === 5) {
            setOffset(offset + 5)
        }
    };

    function decreaseOffset() {
        if (offset > 0) {
            setOffset(offset - 5)

        }
    }

    return (

        <div className="row">

            <div className="col-12 col-lg-9 py-5 align-items-center d-flex flex-column">
                <Articles offset={offset} />
                {articles && <nav aria-label='...'>
                    <MDBPagination circle className='mb-0'>
                        <MDBPaginationItem>
                            {offset !== 0 ? <button onClick={() => decreaseOffset()} className="btn btn-light">Previous</button> : <button disabled className="btn btn-light">Previous</button>}
                        </MDBPaginationItem>
                        <MDBPaginationItem>
                            <MDBBadge className="mt-1 mx-2 bg-warning"><span className="h6">{offset / 5 + 1}</span></MDBBadge>
                        </MDBPaginationItem>
                        <MDBPaginationItem>
                            {articles.length === 5 ? <button onClick={() => increaseOffset()} className="btn btn-light">Next</button> : <button disabled className="btn btn-light">Next</button>}
                        </MDBPaginationItem>
                    </MDBPagination>
                </nav>}
            </div>

            <div className=' py-5  col-12 col-lg-3  '>
                <MDBCard alignment='center' className='mt-2 shadow-4'>
                    <MDBCardBody >
                        <MDBCardTitle className='border-bottom border-info rounded p-1'>Last registered users</MDBCardTitle>
                        <div className=''>
                            {last ? last.map((value, index) => { return <MDBBadge color='info' key={index} className="m-1">{value}</MDBBadge> }) : <MDBSpinner role='status'>
                                <span className='visually-hidden'>Loading...</span>
                            </MDBSpinner>}
                        </div>
                    </MDBCardBody>
                </MDBCard>
            </div>

        </div>
    )
}