import { MDBInput, MDBInputGroup, MDBInputGroupText } from "mdb-react-ui-kit";
import { useState } from "react";
import ReactMarkdown from "react-markdown";
import { useDispatch, useSelector } from "react-redux";
import * as filestack from 'filestack-js';
import MdEditor from 'react-markdown-editor-lite';
import { thisArticle, upArticle } from "../stores/article-slice";
import { useHistory } from "react-router-dom";
import moment from "moment";

export const Update = () => {
    const old = useSelector(state => state.art.updateArt)
    const feedback = useSelector(state => state.art.upFeedback)
    const [image, setImage] = useState({ image: old.image })
    const [change, setChange] = useState({ ...old })
    let dispatch = useDispatch();
    let history = useHistory();

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        setChange({ ...change, [name]: value })
    }

    function handleSubmit(event) {
        dispatch(upArticle({...change, date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')}))
        if (feedback === 'Updated !') {
            dispatch(thisArticle({...change, date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')}))
            return history.push('/article/'+old.articleId)
        }
    
    }


    const client = filestack.init('AREYv4BEcS7Kac5TzhVrGz');
    const options = {
        onFileSelected: file => {
            if (file.size > 1000 * 1000) {
                throw new Error('File too big, select something smaller than 1MB');
            }
        },
        transformations: {
            crop: {
                force: true,
                aspectRatio: 21 / 9
            }
        },
        accept: ["image/*"],
        uploadInBackground: false,
        onUploadDone: handleImage,



    };


    function handleImage(result) {
        setImage(result.filesUploaded[0])
        setChange({ ...change, image: result.filesUploaded[0].url })
    }

    function deleteImage() {
        client.remove(image.handle, { policy: 'eyJleHBpcnkiOjIyNDAxNzU2MDAsImNhbGwiOlsicmVtb3ZlIl19', signature: '4a30599c62c2645fefae24e278072f41bd760eb49154f9c80b96b50eaeb52356' });
        setImage(null)
    }


    function handleEditorChange({ html, text }) {
        setChange({ ...change, text: text });
    }


    return (
        <div className='row g-3 justify-content-center my-3' >
            <div className="col-10 m-4 p-3 bg-light border border-info rounded row">
                <h2 className='text-center mb-3'><span className="border px-2">Update Article</span></h2>
                <div className="col-md-6 col-12 my-2 ">
                    <MDBInput label='Title'  defaultValue={old.title} name='title'
                        onChange={handleChange}
                        required ></MDBInput>
                </div>
                <div className='col-md-6 col-12 my-2 d-flex '>
                    {image === null ? <MDBInputGroup className='mb-3'>
                        <button name="image" className='w-75 btn border text-reset btn-sm btn-link ' onClick={() => client.picker(options).open()}>{'Click to add an image'}</button>
                        <MDBInputGroupText>image</MDBInputGroupText>
                    </MDBInputGroup> :
                        <MDBInputGroup className='mb-3'>
                            <MDBInputGroupText className="col-9 overflow-auto">{image.filename || image.image}</MDBInputGroupText>
                            <button className='w-25 btn border text-reset btn-sm btn-link ' onClick={() => deleteImage()}>Delete</button>
                        </MDBInputGroup>
                    }
                </div>
                <MdEditor name='text' defaultValue={old.text} style={{ height: '30em' }} renderHTML={text => <ReactMarkdown>{text}</ReactMarkdown>} onChange={handleEditorChange} />
                <div className="col-12 mt-3 d-flex justify-content-center">
                    <button className="btn btn-info w-auto" onClick={() => handleSubmit()}>Submit</button>
                </div>
            </div>
        </div>

    )
}